import gspread
from oauth2client.service_account import ServiceAccountCredentials
from apiclient import discovery
import httplib2
import datetime
import telebot
from telebot import types
from config import TOKEN, spreadsheet_id, link
CREDENTIALS_FILE = 'credentials.json'
credentials = ServiceAccountCredentials.from_json_keyfile_name(
    CREDENTIALS_FILE,
    ['https://www.googleapis.com/auth/spreadsheets',
     'https://www.googleapis.com/auth/drive'])
httpAuth = credentials.authorize(httplib2.Http())
service = discovery.build('sheets', 'v4', http=httpAuth)
bot = telebot.TeleBot(TOKEN)
my_creds = ServiceAccountCredentials.from_json_keyfile_name('credentials.json',
                                                            link)
client = gspread.authorize(my_creds)
sheet = client.open('sheet1').sheet1
sheet_col_A = sheet.col_values(1)
sheet_col_B = sheet.col_values(2)
sheet_all = sheet.get_all_values()
now = str(datetime.datetime.now())[:-10]
l = sheet_all[-1]
qqwe = []
qqwe.append(0)
list_ = []


@bot.message_handler(commands=['start'])
def send_welcome(message):
    sheet_all = sheet.get_all_values()
    spis = sheet_col_A
    spis3 = spis[::-1]
    if len(spis) % 2 == 1:
        d = int(0)
        j = int(0)
        summa = int(1)
        for item in sheet_all:
            j += 1
            d += 1
            if len(sheet_col_A) != summa:
                if item[1] == 'done':
                    summa += 1

                else:
                    qqwe[0] += 1
                    list_.append(item[0])
                    if qqwe[0] == 2:
                        qqwe[0] = 0
                        bot.send_message(message.chat.id,
                                         f"The attendants today {list_} ✌️",
                                         reply_markup=keyboard()
                                         )
                        break
            else:
                for item in sheet_all:
                    j += 1
                    d += 1
                    if item[3] == 'done':
                        pass
                    else:
                        qqwe[0] += 1
                        list_.append(item[0])
                        if qqwe[0] == 2:
                            qqwe[0] = 0
                            bot.send_message(message.chat.id,
                                             f"The attendants today:{list_} ✌️",
                                             reply_markup=keyboard()
                                             )

                            break

    else:
        d = int(0)
        j = int(0)
        summa = int(1)
        for item in sheet_all:
            j += 1
            d += 1
            if len(sheet_col_A) != summa:
                if item[1] == 'done':
                    summa += 1

                else:
                    if item[0] == spis[-3]:
                        list_.append(spis[-1])
                        list_.append(spis[-2])
                        list_.append(spis[-3])
                        bot.send_message(message.chat.id,
                                         f"the attendants today: 🔔 {list_}️",
                                         reply_markup=keyboard()
                                         )
                        break
                    else:
                        qqwe[0] += 1
                        list_.append(item[0])
                        if qqwe[0] == 2:
                            qqwe[0] = 0
                            bot.send_message(message.chat.id,
                                             f"The attendants today:{list_}✌️",
                                             reply_markup=keyboard()
                                             )
                            break

            else:
                for item in sheet_all:
                    j += 1
                    d += 1
                    if item[3] == 'done':
                        pass
                    else:
                        if item[0] == spis[-3]:
                            list_.append(spis[-1])
                            list_.append(spis[-2])
                            list_.append(spis[-3])
                            bot.send_message(message.chat.id,
                                             f"The attendants today:🔔 {list_}️",
                                             reply_markup=keyboard()
                                             )
                            break
                        else:
                            qqwe[0] += 1
                            list_.append(item[0])
                            if qqwe[0] == 2:
                                qqwe[0] = 0
                                bot.send_message(
                                    message.chat.id,
                                    f"The attendants today:{list_}✌️",
                                    reply_markup=keyboard()
                                )

                                break

                break


@bot.message_handler(content_types=["text"])
def send_anytext(message):
    spis = sheet_col_A
    spis3 = spis[::-1]
    sheet_all = sheet.get_all_values()
    chat_id = message.chat.id
    if message.text == 'DONE✅':
        text = 'Thanks for the answer: 👌 ✍️\n /start'
        if len(spis) % 2 == 1:
            d = int(0)
            j = int(0)
            for item in sheet_all:
                j += 1
                d += 1
                if item[1] == item[3]:
                    if len(list_) >= 2:
                        if item[0] == list_[0]:
                            sheet.update_cell(j, 2, 'done')
                            sheet.update_cell(j + 1, 2, 'done')
                            sheet.update_cell(d, 3, now)
                            sheet.update_cell(d + 1, 3, now)
                            list_.clear()
                else:
                    if len(list_) >= 2:
                        if item[0] == list_[0]:
                            sheet.update_cell(j, 4, 'done')
                            sheet.update_cell(j + 1, 4, 'done')
                            sheet.update_cell(d, 5, now)
                            sheet.update_cell(d + 1, 5, now)
                            list_.clear()

            bot.send_message(chat_id,text,reply_markup=keyboard())
        else:
            d = int(0)
            j = int(0)
            for item in sheet_all:
                j += 1
                d += 1
                if item[1] == item[3]:
                    if len(list_) >= 2:
                        if item[0] == list_[0]:
                            sheet.update_cell(j, 2, 'done')
                            sheet.update_cell(j + 1, 2, 'done')
                            sheet.update_cell(d, 3, now)
                            sheet.update_cell(d + 1, 3, now)
                            list_.clear()
                        else:
                            if len(list_) == 3 and item[0] == spis3[2]:
                                    sheet.update_cell(j, 2, 'done')
                                    sheet.update_cell(j + 1, 2, 'done')
                                    sheet.update_cell(j + 2, 2, 'done')
                                    sheet.update_cell(d, 3, now)
                                    sheet.update_cell(d + 1, 3, now)
                                    sheet.update_cell(d + 2, 3, now)
                                    list_.clear()
                else:
                    if len(list_) >= 2:
                        if item[0] == list_[0]:
                            sheet.update_cell(j, 4, 'done')
                            sheet.update_cell(j + 1, 4, 'done')
                            sheet.update_cell(d, 5, now)
                            sheet.update_cell(d + 1, 5, now)
                            list_.clear()
                        else:
                            if len(list_) == 3 and item[0] == spis3[2]:
                                sheet.update_cell(j, 4, 'done')
                                sheet.update_cell(j + 1, 4, 'done')
                                sheet.update_cell(j + 2, 4, 'done')
                                sheet.update_cell(d, 5, now)
                                sheet.update_cell(d + 1, 5, now)
                                sheet.update_cell(d + 2, 5, now)
                                list_.clear()

            bot.send_message(chat_id, text, reply_markup=keyboard())

    else:
        if message.text == 'NEXT ❌':
            sheet_all = sheet.get_all_values()
            s = 0
            for item in sheet_all:
                if list_[1] == item[0]:
                    s += 1
                    pass
                elif s == 1:
                    qqwe[0] += 1
                    list_.append(item[0])
                    if qqwe[0] == 2:
                        list_.pop(0)
                        list_.pop(0)
                        qqwe[0] = 0
                        bot.send_message(message.chat.id,
                                         f'The attendants today:{list_} ⏭ \n'
                                         f' /start')

                        return list_


def keyboard():
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton('DONE✅')
    btn2 = types.KeyboardButton('NEXT ❌')
    markup.add(btn1)
    markup.add(btn2)
    return markup

bot.polling(none_stop=True)
